console.log("Hello World");
let num1 = parseInt(prompt("Please provide a number"));
let num2 = parseInt(prompt("Please provide another number"));

if (num1 + num2 < 10) {
  let sumNum = num1 + num2;
  console.warn("The sum of the numbers is less than or equal to 9");
} else if (num1 + num2 >= 10 && num1 + num2 <= 20) {
  let diffNum = num1 - num2;
  alert("The difference of the two numbers is " + diffNum);
} else if (num1 + num2 >= 21 && num1 + num2 <= 30) {
  let productNum = num1 * num2;
  alert("The product of the two numbers is " + productNum);
} else if (num1 + num2 >= 30) {
  let quotientNum = num1 / num2;
  alert("The quotient of the two numbers is " + quotientNum);
}

let fName = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));

if (fName == null || age == null) {
  alert("Are you a time traveler?");
} else if (fName != null && age != null) {
  alert(`Hi ${fName}. You are ${age} years old.`);
}

function isLegalAge(age) {
  if (age >= 18) {
    alert("You are of legal age.");
  } else {
    alert("You are not allowed here.");
  }
}
isLegalAge(age);

switch (age) {
  case 18:
    alert("You are now allowed to party");
    break;
  case 21:
    alert("You are now part of the adult society");
    break;
  case 65:
    alert("We thank you for your contribution to society");
    break;
  default:
    alert("Are you sure you're not an alien?");
}

function errorMessage(age) {
  try {
    alert(isLegalAage(age));
  } catch (error) {
    console.log(typeof error);
    console.warn(error.message);
  } finally {
    alert("alert message");
  }
}

errorMessage(age);
