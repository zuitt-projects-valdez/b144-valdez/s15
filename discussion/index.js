// Operators
/* 
Operators and what they return 
  Add + = sum 
  Substract - = difference
  Multiply * = product 
  Divide / = quotient 
  Modulus % = remainder
*/

function mod() {
  return 9 % 2;
}

console.log(mod());

//Assignment operator (=)

// Arithmetic assignment operator
/* 
  += (addition)
  -= (subtraction)
  *= (multiplication)
  /= (division)
  %= (modulo)
*/
let x = 1;

let sum = 1;
//sum = sum + 1;
sum += 1; //shorthand of the equation ^

console.log(sum);

// Increment and decrement (++, --)
/* 
  operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to (unlike arithmetic assignment operators where you can add/subtract by more than 1 at a time)
*/

let z = 1;

//pre-increment - add the value before we can assign it
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

//predecrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

//comparison operators
/* compares value and returns boolean values

    Equality operator (==) - checks if the values are equal; type coercion 

    compares values and data types 

    strict equality (===) - checks if values and types are equal 

*/

let juan = "juan";
console.log(1 == 1); //these two values are being compared depending on if they are equal; reads as 'is 1 equal to 1?' - returns true or false

console.log(0 == false); // type coercion: binary
console.log("juan" == juan);

//strict equality (===)
console.log(1 === true);

/* 
  Inequality operator (!=)
  - ! - not symbol
  - checks if the values are not equal
  - returns boolean values 
  - strict inequality (!==)
*/

console.log("Inequality Operator");
console.log(1 != 1);

console.log("Juan" != juan); //not equal because of the capitalization

console.log(0 !== false);

// !!! use strict in/equality to avoid type coercion

/* 
  Other compasion operators 

  > - greater than
  < - less than 
  >= - greater than or equal 
  <= - less than or equal 
*/

/* 
  Logical Operators 
    and operator (&&)
      returns true if all operants are true 
        true && true = true 
        false && true = false
        false && false = false 

    or operator(||)
      returns true if at least one operants is true  
      only returns false if both operants are false 
        true || false = true
*/

let isLegalAge = true;
let isRegistered = false;

// and operator
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

//or operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection Control Structures
/* 
  selects certain statements to be executed based on the conditions

  if statement 
    execute a statemet if a specified condition is true 
    syntax:
      if (condition){
        statement/s;
      }

  if/else statement 
    executes a statement if the previous condition returns false 
    syntax:
      if (condition){
        statement/s;
      } 
      else {
        statement/s;
      }
  
  if/else else/if statement 
    syntax: 
      if (condition){
        statement/s;
      } else if(condition){
        statement/s;
      } 
      .
      .
      .
      else if(condition){
        statement/s;
      } else {
        statement/s;
      }
*/

let num1 = -1;
//if statement
if (num1 < 0) {
  console.log("Hello");
}
let num2 = 10;
if (num2 >= 10) {
  console.log("Welcome to Zuitt");
}

let num3 = 5;
if (num3 >= 10) {
  console.log("Number is greater than or equal to 10");
} else {
  console.log("Number is not greater than or equal to 10");
}

// let age = 50;
// if (age > 59) {
//   console.log("Senior age");
// } else {
//   console.log("Invalid age");
// }
/* let age = parseInt(prompt("Please provide age: ")); //values input through the prompt are considered a string so have to convert to a number data type
if (age > 59) {
  alert("Senior age");
} else {
  alert("Invalid age");
} */

/* 
if else else if statement 
  1 - quezon city 
  2 - valenzuela city 
  3 - pasig city 
  4 - taguig 
*/
/* 
let city = parseInt(prompt("Enter a number: "));
if (city === 1) {
  alert("Welome to Quezon City");
} else if (city === 2) {
  alert("Welome to Valenzuela City");
} else if (city === 3) {
  alert("Welome to Pasig City");
} else if (city === 4) {
  alert("Welome to Taguig City");
} else {
  alert("Invalid number");
} */

let message = " ";
function determineTyphoonIntensity(windSpeed) {
  if (windSpeed < 30) {
    return "Not a typhoon yet";
  } else if (windSpeed <= 61) {
    return "Tropical depression detected";
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return "Tropical storm detected";
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return "Severe tropical storm detected";
  } else {
    return "Typhoon detected";
  }
}

message = determineTyphoonIntensity(70);
console.log(message);

/* 
  Ternary operator 
    shorthand version of the if else statement 

    syntax:
      (condition) ? ifTrue : ifFalse
*/

let ternaryResult = 1 < 18 ? "valid" : "invalid";
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge() {
  name = "John";
  return "You are of the legal age limit";
}

function isUnderAge() {
  name = "Jane";
  return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
let legalAge = age >= 18 ? isOfLegalAge() : isUnderAge();
alert("Result of Ternary operator in functions: " + legalAge + ", " + name);

//Switch statement - shorthand of ifelse elseif statement
/* 
  syntax: 
    switch (expression){
      case value1: 
        statement/s;
        break; <- break keyword; stops the expression if the statement is fulfilled 
      case value2:
        statement/s;
        break; 
      case valueN:
        statement/s;
        break;
      default: 
        statement/s;
    }
*/

let day = prompt("What day of the week is it today?").toLowerCase(); //toLowerCase() function sets strings to lowercase letters
switch (day) {
  case "sunday":
    alert("The color of the day is red");
    break;
  case "monday":
    alert("The color of the day is orange");
    break;
  case "tuesday":
    alert("The color of the day is yellow");
    break;
  case "wednesday":
    alert("The color of the day is green");
    break;
  case "thursday":
    alert("The color of the day is blue");
    break;
  case "friday":
    alert("The color of the day is indigo");
    break;
  case "saturday":
    alert("The color of the day is violet");
    break;
  default:
    alert("Please input valid day");
}

/* 
  Try-catch-finally statement 
    commonly used for error handling 
      tries if a block of code will work 
      if not catch will do something idk 
      finally will send a message regardless of if there is an error or not
    used mostly for during development 
*/

function showIntensityAlert(windSpeed) {
  try {
    alerat(determineTyphoonIntensity(windSpeed));
  } catch (error) {
    console.log(typeof error);
    console.warn(error.message);
  } finally {
    alert("Intensity updates will show new alert.");
  }
}

showIntensityAlert(56);
